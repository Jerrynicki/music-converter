# music-converter

A script for converting a music library organized in folders/subfolders to a different format. Useful if you have a lossless music library on your pc but want to sacrifice quality for filesize when transferring it to your phone.

The script copies the directory structure of a start directory to a target directory and converts or copies all files (depending on the specified extension(s)) from the start directory structure to the target directory structure.
