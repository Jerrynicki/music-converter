#!/bin/bash

# Script for converting my lossless music library to a lossy (smaller) format so it doesn't use up all the space on my phone

shopt -s globstar

from="Music"
to="Music_opus"
ffmpeg_args="-c:a libopus -b:a 256k -vn"
convert_from=" flac wav opus " # this can contain multiple values, the spaces at the beginning and end are important for matching (i love bash) 
convert_to="ogg"

echo "Copying directory structure..."
mkdir "$to"
find "$from" -type d -print0 | xargs -0 -I{} mkdir -p "./$to/{}"

i=0
no=$(find "$from"/**/* | wc -l)

echo -n "Found $no files, starting conversion in "
for j in {3..1}; do
	echo -n "$j "
	sleep 1
done
echo ""

for file in "$from"/**/*; do
	i=$((i+1))
	echo "$i/$no"

	# Skip directories
	if [ -d "$file" ];
	then
		echo "(directory) → SKIP ($file)"
		continue
	fi

	base="${file%.*}"
	ext="${file#$base.}"

	echo -n "$ext "

	if [ -f "$to/$base.$convert_to" ] || [ -f "$to/$file" ]; then
		echo -n "exists → SKIP "
	else
		if [[ "$convert_from" == *" $ext "* ]];
		then
			# Convert files in $convert_from
			echo -n "→ CONVERT "
			ffmpeg -hide_banner -loglevel warning -nostats -i "$file" $ffmpeg_args "$to/$base.$convert_to"
		else
			# Copy other files
			echo -n "→ COPY "
			cp "$file" "$to/$file"
		fi
	fi

	echo "($file)"
done

